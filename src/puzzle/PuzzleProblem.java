package puzzle;

import java.util.Scanner;

public class PuzzleProblem {

	@SuppressWarnings("resource")
	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter a string of 25 characters including space");
		String str = sc.nextLine();
		char[][] puzzle;
		if (checkSpaceInString(str) && str.length() == 25) {

			puzzle = getPuzzleArr(str.toUpperCase());

			solvePuzzle(puzzle);
		} else
			System.out.println("Not a valid puzzle");

	}

	static boolean checkSpaceInString(String str) {

		for (int i = 0; i < str.length(); i++) {

			if (Character.isWhitespace(str.charAt(i))) {
				return true;
			}

		}
		return false;

	}

	static void solvePuzzle(char[][] puzzle) {
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		char move = 'Z';
		do {
			if (move == 'A' || move == 'B' || move == 'L' || move == 'R') {
				if (isValidMove(puzzle, move)) {
					puzzle = swap(puzzle, move);
					if (isWin(puzzle)) {
						System.out.println("You Won");
						move = '0';
					}
					display(puzzle);

				} else {
					System.out.println("Not valid move");

				}
			} else
				System.out.println("Enter valid Move i.e A - Above , B - Below , L - Left , R - Right , 0 - Exit Game");
			if(move != '0')
				move = sc.next().charAt(0);

		} while (move != '0');

	}

	static char[][] getPuzzleArr(String str) {

		char[][] puzzle;
		puzzle = new char[5][5];
		int k = 0;

		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 5; j++) {
				puzzle[i][j] = str.charAt(k);
				k++;
			}
		}

		return puzzle;

	}

	static int[] findBlankSpace(char arr[][]) {

		int[] arr1 = new int[2];

		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 5; j++) {

				char temp = arr[i][j];

				if (Character.isWhitespace(temp)) {

					arr1[0] = i;
					arr1[1] = j;

				}

			}
		}

		return arr1;

	}

	static boolean isValidMove(char[][] puzzle, char move) {
		int a[] = new int[2];
		a = findBlankSpace(puzzle);

		if (a[0] == 0 && move == 'A') {
			return false;
		} else if (a[0] == 4 && move == 'B') {
			return false;
		} else if (a[1] == 0 && move == 'L') {
			return false;
		} else if (a[1] == 4 && move == 'R') {
			return false;
		} else
			return true;
	}

	static char[][] swap(char[][] puzzle, char move) {

		int a[] = new int[2];
		char temp = ' ';
		a = findBlankSpace(puzzle);
		if (move == 'R') {
			temp = puzzle[a[0]][a[1]];
			puzzle[a[0]][a[1]] = puzzle[a[0]][a[1] + 1];
			puzzle[a[0]][a[1] + 1] = temp;
		}
		else if (move == 'L') {
			temp = puzzle[a[0]][a[1]];
			puzzle[a[0]][a[1]] = puzzle[a[0]][a[1] - 1];
			puzzle[a[0]][a[1] - 1] = temp;
		}
		else if (move == 'B') {
			temp = puzzle[a[0]][a[1]];
			puzzle[a[0]][a[1]] = puzzle[a[0] + 1][a[1]];
			puzzle[a[0] + 1][a[1]] = temp;
		}
		else if (move == 'A') {
			temp = puzzle[a[0]][a[1]];
			puzzle[a[0]][a[1]] = puzzle[a[0] - 1][a[1]];
			puzzle[a[0] - 1][a[1]] = temp;
		}

		return puzzle;

	}

	static void display(char[][] puzzle) {

		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 5; j++) {
				System.out.print(puzzle[i][j]);
			}
			System.out.println();
		}
		System.out.println("----------------");

	}

	static boolean isWin(char[][] puzzle) {

		char max = 'A';

		if (puzzle[0][0] == ' ') {

			for (int i = 0; i < 5; i++) {
				for (int j = 0; j < 5; j++) {
					if (i != 0 && j != 0) {
						if (puzzle[i][j] >= max) {

							max = puzzle[i][j];

						}

						else {
							return false;
						}
					}

				}
			}

			return true;

		}

		else if (puzzle[4][4] == ' ') {

			for (int i = 0; i < 5; i++) {
				for (int j = 0; j < 5; j++) {

					if (i != 4 && j != 4) {

						if (puzzle[i][j] >= max) {

							max = puzzle[i][j];

						}

						else {
							return false;
						}

					}

				}
			}
			return true;

		} else
			return false;
	}

}
