package puzzle;

import java.util.Scanner;

public class CrossWordPuzzle {

	private static Scanner sc;
	private static Scanner sc2;

	public static void main(String[] args) {

		sc = new Scanner(System.in);
		System.out.println("Enter the size of matrix");
		int rows = sc.nextInt();
		int columns = sc.nextInt();

		char[][] crossWordArray = getCrossWordArray(rows, columns);
		displayCrossWordArray(crossWordArray);

		int[][] numberedArray = getNumberedArray(crossWordArray);
		displayNumberedArray(numberedArray);

		String[] acrossWords = across(crossWordArray, numberedArray);
		displayAcrossWords(acrossWords);

		String[] downWords = down(crossWordArray, numberedArray);
		displayDownWords(downWords);

	}

	static char[][] getCrossWordArray(int rows, int columns) {

		sc2 = new Scanner(System.in);

		char[][] crossWordArray = new char[rows][columns];

		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {

				crossWordArray[i][j] = sc2.next().charAt(0);

			}
			System.out.println();
		}

		return crossWordArray;

	}

	static int[][] getNumberedArray(char[][] crossWordArray) {

		int[][] numberedArray = new int[crossWordArray.length][crossWordArray[0].length];
		int k = 1;

		for (int i = 0; i < numberedArray.length; i++) {
			for (int j = 0; j < numberedArray[i].length; j++) {

				if (((i == 0 || j == 0) && crossWordArray[i][j] != '*')) {

					numberedArray[i][j] = k;
					k++;
				}

				else if (crossWordArray[i][j] != '*'
						&& (crossWordArray[i - 1][j] == '*' || crossWordArray[i][j - 1] == '*')) {

					numberedArray[i][j] = k;
					k++;

				}

			}
		}

		return numberedArray;

	}

	static String[] across(char[][] crossWordArray,int[][] numberedArray) {

		String str = "";

		for (int i = 0; i < crossWordArray.length; i++) {

			for (int j = 0; j < crossWordArray[0].length; j++) {
				
				// Getting numbers for first row and first column
				if ((i == 0 || j == 0) && crossWordArray[i][j] != '*') {

					str += numberedArray[i][j] + ".";

				}

				// Getting numbers when there is a * left to the character
				if (i != 0 && j != 0 && crossWordArray[i][j - 1] == '*') {

					str += numberedArray[i][j] + ".";

				}

				// Getting across words
				while (j < crossWordArray[0].length && crossWordArray[i][j] != '*') {

					str = str + crossWordArray[i][j];
					j++;

				}
				// if((j < rows && crossWordArray[i][j] == '*') || (j ==
				// columns-1 && crossWordArray[i][j] != '*'))
				str = str + " ";
			}

		}
		String acrossWords[] = str.split("\\s+");

		return acrossWords;

	}

	static String[] down(char[][] crossWordArray,int[][] numberedArray) {

		String str = "";

		for (int i = 0; i < crossWordArray.length; i++) {
			int m = i;
			for (int j = 0; j < crossWordArray[0].length; j++) {

				if (i == 0 && crossWordArray[i][j] != '*') {
					
					str += numberedArray[i][j] + ".";

					// Getting down words for first row
					while (i < crossWordArray.length && crossWordArray[i][j] != '*') {

						str = str + crossWordArray[i][j];

						i++;

					}
					str = str + " ";
					i = m;
				}

				else if (crossWordArray[i][j] != '*' && crossWordArray[i - 1][j] == '*') {
					
					str += numberedArray[i][j] + ".";

					// Getting down words for remaining rows
					while (i < crossWordArray.length && crossWordArray[i][j] != '*') {

						str = str + crossWordArray[i][j];

						i++;

					}
					str = str + " ";
					i = m;

				}

			}

		}

		String downWords[] = str.split("\\s+");

		return downWords;
	}

	static void displayCrossWordArray(char[][] crossWordArray) {

		System.out.println("CrossWordArray");
		for (int i = 0; i < crossWordArray.length; i++) {
			for (int j = 0; j < crossWordArray[i].length; j++) {

				System.out.print(crossWordArray[i][j] + "\t");

			}
			System.out.println();
		}

	}

	static void displayNumberedArray(int[][] numberedArray) {

		System.out.println("NumberedArray");
		for (int i = 0; i < numberedArray.length; i++) {
			for (int j = 0; j < numberedArray[i].length; j++) {

				System.out.print(numberedArray[i][j] + "\t");

			}
			System.out.println();
		}
	}

	static void displayAcrossWords(String[] acrossWords) {
		
		System.out.println("Across");
		
		for (int i = 0; i < acrossWords.length; i++)
			System.out.println(acrossWords[i]);

	}
	
	static void displayDownWords(String[] downWords) {
		
		System.out.println("Down");
		
		for (int i = 0; i < downWords.length; i++)
			System.out.println(downWords[i]);

	}

}
